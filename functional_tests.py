from selenium import webdriver
import unittest

class NewVisitorTest(unittest.TestCase):
	def setUp(self):
		self.browser = webdriver.Firefox()

	def tearDown(self):
		self.browser.quit()

	def test_homepage_GUI(self):
		# user open website and see website
		self.browser.get('http://localhost:8000')

		# title of website appear 'Word Bank Website'
		self.assertIn('Word Bank Website', self.browser.title)

		# center top of page appear logo 'Word Bank'
		# 'Word Bank' is in center of page , look like header and bold
		logo = self.browser.find_element_by_id('logo')
		logoCenterTag = logo.find_element_by_tag_name('center')
		logoCenterHeaderTag = logoCenterTag.find_element_by_tag_name('h1')
		logoText = logoCenterHeaderTag.find_element_by_tag_name('b').text
		self.assertIn('Word Bank',logoText)

		# middle page appear text input
		# for input text that you want to search
		searchBox = self.browser.find_element_by_id('searchBox')
		searchBoxCenterTag = searchBox.find_element_by_tag_name('center')
		searchForm = searchBoxCenterTag.find_element_by_id('searchForm')
		wordTextInput = searchForm.find_element_by_id('wordInput')

		# right of text input appear search button
		searchButton = searchForm.find_element_by_id('searchSubmit')

		# bottom of text input appear search method
		# with drop down box for select method
		self.assertIn('Search method :',searchForm.text)
		searchMethodDropDownBox = searchForm.find_element_by_id('searchMethod')

		# in drop down box have 3 method
		# Any , Start with , Match all
		searchMethodDropDownBox.find_element_by_id('any')
		searchMethodDropDownBox.find_element_by_id('startWith')
		searchMethodDropDownBox.find_element_by_id('matchAll')

		# user close browser

	def test_search(self):
		# user open website
		self.browser.get('http://localhost:8000')

		# type 'mas' in search word input
		searchBox = self.browser.find_element_by_id('searchBox')
		searchForm = searchBox.find_element_by_id('searchForm')
		wordTextInput = searchForm.find_element_by_id('wordInput')
		searchingWord = 'Mas'
		wordTextInput.send_keys(searchingWord)
		
		# enter
		searchButton = searchForm.find_element_by_id('searchSubmit')
		searchButton.click()

		# result page appear
		# user see result page
		self._test_result_page_GUI(searchingWord)

		# user see word table
		wordResult = self.browser.find_element_by_id('wordResult')
		wordResultTable = wordResult.find_element_by_id('wordResultTable')

		# It's show user expected word => Master , Mastery

		# Master is Noun
		# It's meaning is เจ้านาย ,ครู , ผู้เชี่ยวชาญ
		masterRow = wordResultTable.find_element_by_id('Master')
		self.assertEqual('Master',masterRow.find_element_by_name('wordText').text)
		masterAllType = wordResultTable.find_elements_by_name('wordType')
		self.assertTrue(any(t.text == 'Noun' for t in masterAllType))

		masterAllMeaning = wordResultTable.find_elements_by_name('wordMeaning')
		expectedMasterMeaning = ['เจ้านาย' ,'ครู' , 'ผู้เชี่ยวชาญ']
		for exMeaning in expectedMasterMeaning:
			self.assertTrue(any(meaning.text == exMeaning for meaning in masterAllMeaning))

		# Mastery is Noun
		# It's meaning is ความเชี่ยวชาญ , การบังคับบัญชา
		masteryRow = wordResultTable.find_element_by_id('Mastery')
		self.assertEqual('Mastery',masteryRow.find_element_by_name('wordText').text)
		masteryAllMeaning = wordResultTable.find_elements_by_name('wordMeaning')
		expectedMasteryMeaning = ['ความเชี่ยวชาญ' , 'การบังคับบัญชา']
		for exMeaning in expectedMasteryMeaning:
			self.assertTrue(any(meaning.text == exMeaning for meaning in masteryAllMeaning))

		# user close browser

	def _test_result_page_GUI(self,searchingWord):
		##this test should run in test_search
		
		#user see result page

		# title of website appear 'Word Bank Website'
		self.assertIn('Word Bank Website', self.browser.title)

		#left-top of page have search panel like homepage
		searchBox = self.browser.find_element_by_id('searchBox')
		searchForm = searchBox.find_element_by_id('searchForm')
		wordTextInput = searchForm.find_element_by_id('wordInput')
		searchButton = searchForm.find_element_by_id('searchSubmit')
		#in word input display searching word
		self.assertEqual(wordTextInput.get_attribute('value'),searchingWord)
		

		#middle of page display result of search word
		resultDisplay = self.browser.find_element_by_id('wordResult')
		#it show by table
		resultTable = resultDisplay.find_element_by_id('wordResultTable')
		#header columns of table are Word , Type , Meaning , Example sentences
		resultTableHeaderRow = resultTable.find_element_by_id('header')
		correctResultTableHeader = ['Word','Type','Meaning','Example sentences']
		resultTableHeaderRowList = resultTableHeaderRow.find_elements_by_tag_name('th')
		for i in range(len(correctResultTableHeader)):
			self.assertEqual(resultTableHeaderRowList[i].text,correctResultTableHeader[i])
			
		#in Type column user see only
		#Type , Noun , Pronoun , Verb , Adverb , Adjective , Preposition , Conjunction , Interjection
		correctType = ['Type', 'Noun' , 'Pronoun' , 'Verb' , 'Adverb' , 'Adjective' , 'Preposition' , 'Conjunction' , 'Interjection']
		dataColumnTypeList = resultTable.find_elements_by_name('wordType')
		for data in dataColumnTypeList:
			self.assertTrue(any(data.text == t for t in correctType))


if __name__ == '__main__':
	unittest.main(warnings='ignore')
