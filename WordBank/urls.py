from django.conf.urls import url

from . import views

app_name = 'WordBank'
urlpatterns = [
    url(r'^$', views.index, name='index'),
    url(r'^result/$', views.result, name='result'),
    url(r'^addword/$', views.addword, name='addword'),
    url(r'^allword/$', views.allword, name='allword'),
    url(r'^backupandrestore/$', views.backup_and_restore, name='backupandrestore'),
    url(r'^showlastsearchkeyword/$', views.showLastSearchKeyword, name='showlastsearchkeyword'),
    url(r'^showmostsearchword/$', views.showMostSearchWord, name='showmostsearchword'),
]
