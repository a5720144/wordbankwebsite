from django.db import models

class Word(models.Model):
	word_text = models.CharField(max_length=100)
	def __str__(self):
		return self.word_text

class WordType(models.Model):
	word = models.ForeignKey(Word, on_delete=models.CASCADE)
	word_type = models.CharField(max_length=15)
	def __str__(self):
		return "{} ({})".format(self.word.word_text,self.word_type)

class WordMeaning(models.Model):
	type = models.ForeignKey(WordType, on_delete=models.CASCADE)
	word_meaning = models.CharField(max_length=200)
	def __str__(self):
		return "{} ({}) = {}".format(self.type.word.word_text,self.type.word_type,self.word_meaning)

class WordExample(models.Model):
	meaning = models.ForeignKey(WordMeaning, on_delete=models.CASCADE)
	word_example = models.CharField(max_length=200)
	def __str__(self):
		return "{} ({}) ex. {}".format(self.meaning.type.word.word_text,self.meaning.type.word_type,self.word_example)


	
