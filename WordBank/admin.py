from django.contrib import admin

from .models import Word , WordType , WordMeaning , WordExample
# Register your models here.

admin.site.register(Word)
admin.site.register(WordType)
admin.site.register(WordMeaning)
admin.site.register(WordExample)
