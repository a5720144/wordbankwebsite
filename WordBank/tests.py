from django.core.urlresolvers import resolve
from django.test import TestCase
from django.http import HttpRequest
from django.template.loader import render_to_string

from WordBank.views import index

class IndexPageTest(TestCase):
	def test_root_url_resolve_to_index_view(self):
		found = resolve('/')
		self.assertEqual(found.func, index)

	def test_index_view_return_correct_html(self):
		request = HttpRequest()
		response = index(request)
		expected_html = render_to_string('WordBank/index.html')
		self.assertEqual(response.content.decode(), expected_html)

