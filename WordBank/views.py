from django.shortcuts import render
from django.http import HttpRequest , HttpResponse , HttpResponseRedirect
from django.template import loader
from django.core.urlresolvers import reverse
from django.core.files.storage import default_storage
from django.utils.encoding import smart_str
from wsgiref.util import FileWrapper
from django.utils import timezone

import csv , os , time , datetime
from xml.etree import ElementTree as ET

from .models import Word , WordType , WordMeaning , WordExample

def index(request):
	return render(request, 'WordBank/index.html', {})

def result(request):
	searchWord = request.GET['wordInput']
	searchMethod = request.GET['searchMethod']
	if searchMethod == "any":
		wordList = Word.objects.filter(word_text__contains=searchWord)
	elif searchMethod == "startWith":
		wordList = Word.objects.filter(word_text__startswith=searchWord)
	else:
		searchList = Word.objects.filter(word_text__startswith=searchWord)
		wordList = []
		for w in searchList:
			if w.word_text == searchWord:
				wordList.append(w)

	# save last search keyword
	lastSearchDirPath = 'WordBank/'
	lastSearchFileName = 'lastSearchKeyword'
	tree = ET.parse('{}{}.xml'.format(lastSearchDirPath,lastSearchFileName))
	root = tree.getroot()
	keyword_ele = ET.SubElement(root, "Search_Keyword")
	keyword_ele.text = searchWord
	time_ele = ET.SubElement(keyword_ele, "Search_Time")
	time_ele.text = "{}/{}/{}".format(timezone.now().day,timezone.now().month,timezone.now().year)
	wordFoundCount_ele = ET.SubElement(keyword_ele, "Found")
	wordFoundCount_ele.text = "{} words".format(len(wordList))
	foundWord_ele = ET.SubElement(keyword_ele, "Found_Word")
	foundWordList = [] #for most search word
	for w in wordList:
		wordFound_ele = ET.SubElement(foundWord_ele, "Word")
		wordFound_ele.text = w.word_text
		foundWordList.append(w.word_text)
	#save most search word
	mostSearchDirPath = 'WordBank/'
	mostSearchFileName = 'mostSearchWord'
	mostSearchTree = ET.parse('{}{}.xml'.format(mostSearchDirPath,mostSearchFileName))
	mostSearchRoot = mostSearchTree.getroot()
	updatedSearchWordList = []
	print(foundWordList)
	for word in mostSearchRoot:
		for foundWord in foundWordList:
			if word.text == foundWord:
				searchTimes_ele = word.find("searchTimes")
				newSearchTimes = int(searchTimes_ele.text) +1
				searchTimes_ele.text = str(newSearchTimes)
				updatedSearchWordList.append(foundWord)
	print(updatedSearchWordList)
	for updatedSearchWord in updatedSearchWordList:
		foundWordList.remove(updatedSearchWord)
	print(foundWordList)
	for unupdateSearchWord in foundWordList:
		word_ele = ET.SubElement(mostSearchRoot, "word")
		word_ele.text = unupdateSearchWord
		searchTimes_ele = ET.SubElement(word_ele, "searchTimes")
		searchTimes_ele.text = "1"
	with open("{}{}.xml".format(mostSearchDirPath,mostSearchFileName), 'wb') as xmlfile:
		mostSearchTree.write(xmlfile, encoding='utf-8')
	# keep only last ten search keyword
	for searchKeyword in root:
		if len(root.findall("Search_Keyword")) > 10:
			root.remove(searchKeyword)
	with open("{}{}.xml".format(lastSearchDirPath,lastSearchFileName), 'wb') as xmlfile:
		tree.write(xmlfile, encoding='utf-8')

	return render(request, 'WordBank/result.html', {'wordInput':searchWord , 'wordList':wordList})

def getLastSearchKeyword(request):
	lastSearchDirPath = 'WordBank/'
	lastSearchFileName = 'lastSearchKeyword'
	tree = ET.parse('{}{}.xml'.format(lastSearchDirPath,lastSearchFileName))
	

def addword(request):
	context = {'message_add':'','message_upload':''}
	if request.method == 'POST':
		if request.FILES == {}:
			word = request.POST["word"]
			typ = request.POST["wordType"]
			meaning = request.POST["wordMeaning"]
			examples = request.POST["wordExample"]
			examples = examples.split("\n")
			if word!=''or meaning!='': #empty fill
				if Word.objects.filter(word_text=word): #if have word
					word_add = Word.objects.get(word_text=word) #get word
				else: #if not have word
					word_add = Word(word_text=word) #create new word
				word_add.save()
				if word_add.wordtype_set.filter(word_type__startswith=typ): #if have typ
					word_add_type = word_add.wordtype_set.get(word_type=typ) #get type
				else: #if not have type
					word_add_type = word_add.wordtype_set.create(word_type=typ) #create new type
				word_add_type.save()
				if word_add_type.wordmeaning_set.filter(word_meaning__startswith=meaning): #if have typ
					word_add_meaning = word_add_type.wordmeaning_set.get(word_meaning=meaning) #get type
				else: #if not have type
					word_add_meaning = word_add_type.wordmeaning_set.create(word_meaning=meaning) #create new type
				word_add_meaning.save()
				old_word_example_list = word_add_meaning.wordexample_set.all()
				for example in examples:
					if example != "":
						isNotHaveExample = True
						for oldexample in old_word_example_list:
							if example == oldexample.word_example:
								isHaveExample = False
						if isNotHaveExample:
							word_add_example = word_add_meaning.wordexample_set.create(word_example=example)
							word_add_example.save()
				context = {'message_add':'Add word success','message_upload':''}
			else:
				context = {'message_add':'Please fill input','message_upload':''}
		elif request.POST['submit'] == "Upload CSV File":
			context = upload_word_csv(request)
		elif request.POST['submit'] == "Upload XML File":
			context = upload_word_xml(request)
	return render(request, 'WordBank/addword.html',context)

def allword(request):
	wordList = Word.objects.filter(word_text__startswith="")
	return render(request, 'WordBank/allword.html', {'wordList':wordList})

def upload_word_csv(request,csvfilename='word_data'):
	if csvfilename == 'word_data':
		dirPath = 'WordBank/wordCSV/'
		csvFile = request.FILES['csvFile']
		saveFilePath = default_storage.save(dirPath, csvFile)
		os.rename(saveFilePath,"{}word_data.csv".format(dirPath))
	else:
		dirPath = 'WordBank/backup/'
	with open("{}{}.csv".format(dirPath,csvfilename),encoding='utf-8') as csvfile:
		dataReader = csv.DictReader(csvfile)
		word_add = None
		word_add_type = None
		word_add_meaning = None
		word_add_example = None
		for row in dataReader:
			if row["Word"] != "":
				word = row["Word"]
				if Word.objects.filter(word_text=word): #if have word
					word_add = Word.objects.get(word_text=word) #get word
				else: #if not have word
					word_add = Word(word_text=word) #create new word
				word_add.save()
			if row["Type"] != "":
				typ = row["Type"]
				if word_add.wordtype_set.filter(word_type__startswith=typ): #if have typ
					word_add_type = word_add.wordtype_set.get(word_type=typ) #get type
				else: #if not have type
					word_add_type = word_add.wordtype_set.create(word_type=typ) #create new type
				word_add_type.save()
			if row["Meaning"] != "":
				meaning = row["Meaning"]
				if word_add_type.wordmeaning_set.filter(word_meaning__startswith=meaning): #if have typ
					word_add_meaning = word_add_type.wordmeaning_set.get(word_meaning=meaning) #get type
				else: #if not have type
					word_add_meaning = word_add_type.wordmeaning_set.create(word_meaning=meaning) #create new type
				word_add_meaning.save()
			if row["Example Sentences"] != "":
				example = row["Example Sentences"]
				if not(word_add_meaning.wordexample_set.filter(word_example__startswith=example)):
					word_add_example = word_add_meaning.wordexample_set.create(word_example=example)
					word_add_example.save()
	context = {'message_add':'','message_upload_csv':'Upload word success'}
	return context

def upload_word_xml(request):
	dirPath = 'WordBank/temp/'
	xmlFile = request.FILES['xmlFile']
	saveFilePath = default_storage.save(dirPath, xmlFile)
	os.rename(saveFilePath,"{}word_upload.xml".format(dirPath))
	tree = ET.parse('{}word_upload.xml'.format(dirPath))
	root = tree.getroot()
	for w in root:
		word = w.attrib['text']
		if Word.objects.filter(word_text=word): #if have word
			word_add = Word.objects.get(word_text=word) #get word
		else:
			word_add = Word(word_text=word)
			word_add.save()
		for t in w:
			typ = t.attrib['text']
			if word_add.wordtype_set.filter(word_type__startswith=typ): #if have typ
				word_add_type = word_add.wordtype_set.get(word_type=typ) #get type
			else:
				word_add_type = word_add.wordtype_set.create(word_type=typ)
				word_add_type.save()
			for m in t:
				meaning = m.attrib['text']
				if word_add_type.wordmeaning_set.filter(word_meaning__startswith=meaning): #if have typ
					word_add_meaning = word_add_type.wordmeaning_set.get(word_meaning=meaning) #get type
				else:
					word_add_meaning = word_add_type.wordmeaning_set.create(word_meaning=meaning)
					word_add_meaning.save()
				for e in m:
					example = e.text
					if not(word_add_meaning.wordexample_set.filter(word_example__startswith=example)):
						word_add_example = word_add_meaning.wordexample_set.create(word_example=example)
						word_add_example.save()
	context = {'message_add':'','message_upload_xml':'Upload word success'}
	return context

def backup_and_restore(request):
	context = {'message_backup':'','message_restore':'','backup_list':''}
	if request.method == 'POST':
		if request.POST != {}:
			for keys in request.POST.keys():
				if keys == 'backup_csv':
					context = backup(request,'csv')
				elif keys == 'backup_xml':
					context = backup(request,'xml')
				elif keys == 'restore':
					context = restore(request)
				elif keys == 'export':
					return export(request)
				elif keys == 'convertXMLstandard':
					return convertXMLstandard(request)
	#load restore list
	dirPath = 'WordBank/backup/'
	backupList = []
	for file in os.listdir(dirPath):
		filename = file.split("_")
		date = filename[0]
		time = filename[1]
		backupName = ''
		for i in range(2,len(filename)):
			backupName = "{}{}".format(backupName,filename[i])
		#correct format for display file name
		date = date.split('-')
		time = time.split('-')
		backupName = backupName.split('.')
		name=''
		for i in range(len(backupName)-1):
			name = "{}{}".format(name,backupName[i])
		backupType = backupName[len(backupName)-1]
		backupFileName = '{}/{}/{} {}:{} {} Type: {}'.format(date[0],date[1],date[2],time[0],time[1],name,backupType)
		backupList.append(backupFileName)
	context["backup_list"] = backupList
	return render(request,'WordBank/backupandrestore.html',context)

def backup(request,backup_type):
	dirPath = 'WordBank/backup/'
	dateandtime = time.strftime("%d-%m-%Y_%H-%M")
	filename = "{}_{}".format(dateandtime,request.POST["backup_name"])
	if backup_type == 'csv':
		with open("{}{}.csv".format(dirPath,filename), 'w',encoding='utf-8') as csvfile:
			header = ['Word', 'Type', 'Meaning', 'Example Sentences']
			writer = csv.DictWriter(csvfile, fieldnames=header)
			writer.writeheader()
			wordList = Word.objects.all()
			for w in range(len(wordList)):
				typeList = wordList[w].wordtype_set.all()
				for t in range(len(typeList)):
					meaningList = typeList[t].wordmeaning_set.all()
					for m in range(len(meaningList)):
						exampleList = meaningList[m].wordexample_set.all()
						for e in range(len(exampleList)):
							if t == 0:
								word = wordList[w].word_text
							else:
								word = ""
							if m == 0:
								type = typeList[t].word_type
							else:
								word = ""
								type = ""
							if e == 0:
								meaning = meaningList[m].word_meaning
							else:
								word = ""
								type = ""
								meaning = ""
							example = exampleList[e].word_example
							writer.writerow({'Word':word,'Type':type,'Meaning':meaning,'Example Sentences':example})
	elif backup_type == 'xml':
		root = ET.Element("All_Word")
		wordList = Word.objects.all()
		for w in wordList:
			word_ele = ET.SubElement(root, "Word")
			word_ele.set("text",w.word_text)
			typeList = w.wordtype_set.all()
			for t in typeList:
				type_ele = ET.SubElement(word_ele, "Type")
				type_ele.set("text",t.word_type)
				meaningList = t.wordmeaning_set.all()
				for m in meaningList:
					meaning_ele = ET.SubElement(type_ele, "Meaning")
					meaning_ele.set("text",m.word_meaning)
					exampleList = m.wordexample_set.all()
					for e in exampleList:
						example_ele = ET.SubElement(meaning_ele, "Example")
						example_ele.text = e.word_example
		tree = ET.ElementTree(root)
		with open("{}{}.xml".format(dirPath,filename), 'wb') as xmlfile:
			tree.write(xmlfile, encoding='utf-8')
	else:
		return {'message_backup':'Backup Failed','message_restore':'','backup_list':''}
	return {'message_backup':'Backup Success','message_restore':'','backup_list':''}

def restore(request):
	if request.method == 'POST':
		if request.POST != {}:
			backup_string = request.POST['selectBackup']
			#parsing
			backup_string = backup_string.split(" ")
			date = backup_string[0].split("/")
			time = backup_string[1].split(":")
			backup_name = ''
			#if have space in file name
			if len(backup_string) > 5:
				for i in range(2,len(backup_string)-2):
					if i == 2:
						backup_name = "{}{}".format(backup_name,backup_string[i])
					elif i < len(backup_string)-2:
						backup_name = "{}{}{}".format(backup_name,' ',backup_string[i])
					else:
						backup_name = "{}{}".format(backup_name,backup_string[i])
			#if dont have space in file name
			else:
				backup_name = backup_string[2]
			backup_type = backup_string[len(backup_string)-1]
			filename = "{}-{}-{}_{}-{}_{}".format(date[0],date[1],date[2],time[0],time[1],backup_name)
			#delete old data
			for w in Word.objects.all():
				w.delete()
			#add new data	
			if backup_type == 'csv':
				context_return = upload_word_csv(request,filename)
				if context_return['message_upload_csv'] != 'Upload word success':
					return {'message_backup':'','message_restore':'Restore Failed','backup_list':''}
			elif backup_type == 'xml':
				dirPath = 'WordBank/backup/'
				tree = ET.parse('{}{}.xml'.format(dirPath,filename))
				root = tree.getroot()
				for w in root:
					word_add = Word(word_text=w.attrib['text'])
					word_add.save()
					for t in w:
						word_add_type = word_add.wordtype_set.create(word_type=t.attrib['text'])
						word_add_type.save()
						for m in t:
							word_add_meaning = word_add_type.wordmeaning_set.create(word_meaning=m.attrib['text'])
							word_add_meaning.save()
							for e in m:
								word_add_example = word_add_meaning.wordexample_set.create(word_example=e.text)
								word_add_example.save()
			else:
				return {'message_backup':'','message_restore':'Restore Failed','backup_list':''}
			return {'message_backup':'','message_restore':'Restore Success','backup_list':''}
		else:
			return {'message_backup':'','message_restore':'Please select backup for restore','backup_list':''}
	else:
		return {'message_backup':'','message_restore':'','backup_list':''}

def export(request):
	if request.method == 'POST':
		if request.POST != {}:
			backup_string = request.POST['selectBackup']
			#parsing
			backup_string = backup_string.split(" ")
			date = backup_string[0].split("/")
			time = backup_string[1].split(":")
			backup_name = ''
			backup_type = backup_string[len(backup_string)-1]
			#if have space in file name
			if len(backup_string) > 5:
				for i in range(2,len(backup_string)-2):
					if i == 2:
						backup_name = "{}{}".format(backup_name,backup_string[i])
					elif i < len(backup_string)-2:
						backup_name = "{}{}{}".format(backup_name,' ',backup_string[i])
					else:
						backup_name = "{}{}".format(backup_name,backup_string[i])
			#if dont have space in file name
			else:
				backup_name = backup_string[2]
			backup_type = backup_string[len(backup_string)-1]
			filename = "{}-{}-{}_{}-{}_{}".format(date[0],date[1],date[2],time[0],time[1],backup_name)
			#send file to client
			dirPath = 'WordBank/backup/'
			filename = "{}.{}".format(filename,backup_type)
			exportFile = open("{}{}".format(dirPath,filename),'r')
			wrapper = FileWrapper(exportFile)
			response = HttpResponse(wrapper, content_type='application/force-download')
			response['Content-Disposition'] = 'attachment; filename={}'.format(smart_str(filename))
			response['X-Sendfile'] = smart_str("{}{}".format(dirPath,filename))
			response['Content-Length'] = os.path.getsize("{}{}".format(dirPath,filename))
			return response

def convertXMLstandard(request):
	if request.method == 'POST':
		if request.POST != {}:
			dirPath = 'WordBank/temp/'
			xmlFile = request.FILES['xmlFile']
			saveFilePath = default_storage.save(dirPath, xmlFile)
			os.rename(saveFilePath,"{}sourceForConvert.xml".format(dirPath))
			if request.POST["convertType"] == "g2standard":
				sourceTree = ET.parse('{}sourceForConvert.xml'.format(dirPath))
				sourceRoot = sourceTree.getroot()
				convertedRoot = ET.Element("allword")
				for w in sourceRoot:
					for t in w:
						for m in t:
							for e in m:
								word_ele = ET.SubElement(convertedRoot, "word")
								text_ele = ET.SubElement(word_ele, "text")
								text_ele.text = w.attrib["text"]
								type_ele = ET.SubElement(word_ele, "type")
								type_ele.text = t.attrib["text"]
								meaning_ele = ET.SubElement(word_ele, "meaning")
								meaning_ele.text = m.attrib["text"]
								example_ele = ET.SubElement(word_ele, "example")
								example_ele.text = e.text
				convertedTree = ET.ElementTree(convertedRoot)
				outputfilename = "word_g2standard_format.xml"
				with open("{}{}".format(dirPath,outputfilename), 'wb') as xmlfile:
					convertedTree.write(xmlfile, encoding='utf-8')
			elif request.POST["convertType"] == "normal":
				sourceTree = ET.parse('{}sourceForConvert.xml'.format(dirPath))
				sourceRoot = sourceTree.getroot()
				convertedRoot = ET.Element("All_Word")
				for sourceWord in sourceRoot:
					# check word is first found ?
					isFirstFound = True
					word_ele = None
					for word_founded in convertedRoot:
						if word_founded.attrib['text'] == sourceWord.find('text').text:
							isFirstFound = False
							word_ele = word_founded
					if isFirstFound:
						word_ele = ET.SubElement(convertedRoot, "Word")
						source_text_ele = sourceWord.find('text')
						word_ele.set("text",source_text_ele.text)
						source_type_ele = sourceWord.find('type')
						type_ele = ET.SubElement(word_ele, 'Type')
						type_ele.set("text",source_type_ele.text)
						source_meaning_ele = sourceWord.find('meaning')
						meaning_ele = ET.SubElement(type_ele, 'Meaning')
						meaning_ele.set("text",source_meaning_ele.text)
						source_example_ele = sourceWord.find('example')
						example_ele = ET.SubElement(meaning_ele, 'Example')
						example_ele.text = source_example_ele.text
					else:
						type_ele = None
						for t in word_ele.findall('Type'):
							if t.attrib['text'] == sourceWord.find('type').text:
								type_ele = t
						if type_ele == None:
							type_ele = ET.SubElement(word_ele, "Type")
							type_ele.set("text",sourceWord.find('type').text)
						meaning_ele = None
						for m in type_ele.findall('Meaning'):
							if m.attrib['text'] == sourceWord.find('meaning').text:
								meaning_ele = m
						if meaning_ele == None:
							meaning_ele = ET.SubElement(type_ele, "Meaning")
							meaning_ele.set("text",sourceWord.find('meaning').text)
						example_ele = None
						for e in meaning_ele.findall('Example'):
							if e.text == sourceWord.find('example').text:
								example_ele = e
						if example_ele == None:
							example_ele = ET.SubElement(meaning_ele, "Example")
							example_ele.text = sourceWord.find('example').text
				convertedTree = ET.ElementTree(convertedRoot)
				outputfilename = "word_normal_format.xml"
				with open("{}{}".format(dirPath,outputfilename), 'wb') as xmlfile:
					convertedTree.write(xmlfile, encoding='utf-8')
			outputFile = open("{}{}".format(dirPath,outputfilename),'r')
			wrapper = FileWrapper(outputFile)
			response = HttpResponse(wrapper, content_type='application/force-download')
			response['Content-Disposition'] = 'attachment; filename={}'.format(smart_str(outputfilename))
			response['X-Sendfile'] = smart_str("{}{}".format(dirPath,outputfilename))
			response['Content-Length'] = os.path.getsize("{}{}".format(dirPath,outputfilename))
			return response

def showLastSearchKeyword(request):
	lastSearchDirPath = 'WordBank/'
	lastSearchFileName = 'lastSearchKeyword'
	tree = ET.parse('{}{}.xml'.format(lastSearchDirPath,lastSearchFileName))
	root = tree.getroot()
	searchKeywordList = []
	for searchKeyword in root:
		searchKey = {}
		searchKey["Search_Keyword"] = searchKeyword.text
		searchKey["Search_Time"] = searchKeyword.find("Search_Time").text
		searchKey["Found"] = searchKeyword.find("Found").text
		foundWordList = []
		for word in searchKeyword.find("Found_Word"):
			foundWordList.append(word.text)
		searchKey["Found_Word"] = foundWordList
		searchKeywordList.append(searchKey)
	#flip list for lastest search keyword on top
	temp = []
	for i in range(len(searchKeywordList)):
		temp.append(searchKeywordList[len(searchKeywordList)-1-i])
	searchKeywordList = temp
	return render(request, 'WordBank/showLastSearchKeyword.html', {'searchKeywordList':searchKeywordList})

def showMostSearchWord(request):
	mostSearchDirPath = 'WordBank/'
	mostSearchFileName = 'mostSearchWord'
	tree = ET.parse('{}{}.xml'.format(mostSearchDirPath,mostSearchFileName))
	root = tree.getroot()
	mostSearchWordList = []
	for word in root:
		temp = {}
		temp["word"] = word.text
		temp["searchTimes"] = int(word.find("searchTimes").text)
		mostSearchWordList.append(temp)
	#sorting
	for j in range(len(mostSearchWordList)):
		for k in range(j+1,len(mostSearchWordList)):
			if mostSearchWordList[j]["searchTimes"] < mostSearchWordList[k]["searchTimes"]:
				temp = mostSearchWordList[j]
				mostSearchWordList[j] = mostSearchWordList[k]
				mostSearchWordList[k] = temp
	return render(request, 'WordBank/showMostSearchWord.html', {'mostSearchWordList':mostSearchWordList})
