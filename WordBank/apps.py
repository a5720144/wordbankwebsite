from django.apps import AppConfig


class WordbankConfig(AppConfig):
    name = 'WordBank'
